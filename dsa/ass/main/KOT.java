package dsa.ass.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class KOT extends JFrame {

	private JPanel contentPane;
	private JTextField txtDugd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KOT frame = new KOT();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public KOT() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setForeground(Color.GREEN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblKot = new JLabel("Tables");
		lblKot.setForeground(SystemColor.inactiveCaptionText);
		lblKot.setBackground(SystemColor.windowText);
		lblKot.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblKot.setBounds(150, 11, 116, 32);
		contentPane.add(lblKot);
		
		JButton t1 = new JButton("T 1");
		t1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Foods fdFoods=new Foods();
				
				t1.setBackground(Color.YELLOW);
				fdFoods.setVisible(true);
				
			}
		});
		t1.setFont(new Font("Tahoma", Font.BOLD, 13));
		t1.setBounds(10, 75, 72, 40);
		contentPane.add(t1);
		
		JButton button = new JButton("T 1");
		button.setFont(new Font("Tahoma", Font.BOLD, 13));
		button.setBounds(98, 75, 72, 40);
		contentPane.add(button);
		
		JButton button_1 = new JButton("T 1");
		button_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_1.setBounds(183, 75, 72, 40);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("T 1");
		button_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_2.setBounds(10, 133, 72, 40);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("T 1");
		button_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_3.setBounds(98, 133, 72, 40);
		contentPane.add(button_3);
		
		JButton button_4 = new JButton("T 1");
		button_4.setFont(new Font("Tahoma", Font.BOLD, 13));
		button_4.setBounds(183, 133, 72, 40);
		contentPane.add(button_4);
		
		JButton btnDelivered = new JButton("Delivered");
		btnDelivered.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnDelivered.setBounds(328, 198, 89, 23);
		contentPane.add(btnDelivered);
		
		txtDugd = new JTextField();
		txtDugd.setText("dugd");
		txtDugd.setBounds(321, 121, 103, 67);
		contentPane.add(txtDugd);
		txtDugd.setColumns(10);
	}
}
