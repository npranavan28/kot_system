package dsa.ass.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Kitchen extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Kitchen frame = new Kitchen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Kitchen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 530, 300);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblKitchenmanagement = new JLabel("KItchenManagement");
		lblKitchenmanagement.setForeground(SystemColor.textHighlight);
		lblKitchenmanagement.setFont(new Font("TneriTSC", Font.BOLD, 26));
		lblKitchenmanagement.setBounds(77, 24, 348, 27);
		contentPane.add(lblKitchenmanagement);
		
		textField = new JTextField();
		textField.setBounds(33, 78, 211, 98);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnPrepared = new JButton("Delay");
		btnPrepared.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnPrepared.setBounds(314, 149, 89, 39);
		contentPane.add(btnPrepared);
		
		JButton btnGetit = new JButton("GetIT");
		btnGetit.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnGetit.setBounds(85, 204, 89, 23);
		contentPane.add(btnGetit);
		
		JButton button = new JButton("PrePared");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KOT kot = new KOT();
				kot.setVisible(true);
				kot.setBackground(Color.GREEN);
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 12));
		button.setBounds(314, 90, 89, 34);
		contentPane.add(button);
	}

}
