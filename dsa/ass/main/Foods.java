package dsa.ass.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Choice;

public class Foods extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Foods frame = new Foods();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Foods() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 584, 424);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFoodsAndDrinks = new JLabel("FOODS AND DRINKS");
		lblFoodsAndDrinks.setFont(new Font("Sylfaen", Font.BOLD, 30));
		lblFoodsAndDrinks.setBounds(32, 11, 344, 34);
		contentPane.add(lblFoodsAndDrinks);
		
		textField = new JTextField();
		textField.setBounds(53, 112, 116, 26);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("submit");
		btnNewButton.setBounds(359, 114, 89, 23);
		contentPane.add(btnNewButton);
		
		Choice choice = new Choice();
		choice.setBounds(230, 118, 28, 20);
		contentPane.add(choice);
	}
}
